using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerMovement : MonoBehaviour {

    [SerializeField] private bool isGrounded;
    [SerializeField] private float groundedCheckDistance;
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private float gravity;
    [SerializeField] private float jumpHeight;
    private Vector3 velocity;


    private CharacterController controller;
    public Transform cam;
    private float speed = 6f;
    private float turnSmoothVelocity;
    public int health = 5;
    public int stamina = 5;

    public GameObject jumpSound;
    private AudioSource jumpAudio;
    public GameObject itemSound;
    private AudioSource itemAudio;
    void Start() {
        controller = GetComponent<CharacterController>();
        jumpAudio = jumpSound.GetComponent<AudioSource>();
        itemAudio = itemSound.GetComponent<AudioSource>();
    }
    void Update() {
        PlayerIsDead();

        isGrounded = Physics.CheckSphere(transform.position, groundedCheckDistance, groundMask);
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        if (isGrounded) {
            if (direction.magnitude > 0) { // WASD movement
                float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, 0.1f);
                Vector3 moveDir = Quaternion.Euler(0, angle, 0) * Vector3.forward;
                transform.rotation = Quaternion.Euler(0f, angle, 0f);
                controller.Move(moveDir * speed * Time.deltaTime);
            }
            if (Input.GetKeyDown(KeyCode.Space)) { // Space to jump
                //jumpNumber++;
                if (stamina <= 0) // if stamina is depleted the player will lose health instead.
                {
                    health--;
                }
                else
                {
                    stamina--;
                }
                jumpAudio.Play(0);
                velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
            }
        }
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Health") // Health pack
        {
            health = 5;
        }
        else if (other.gameObject.tag == "Coin") // Coin increases stamina by 5
        {
            stamina = 5;
        }
        itemAudio.Play(0);
        Destroy(other.gameObject);
    }

    public void PlayerIsDead()
    {
        if (health <= 0) // Player died : Return to menu
        {
            SceneManager.LoadScene("StartScreen");
        }
    }
}
