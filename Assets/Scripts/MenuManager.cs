using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject quitMenu;

    // Start is called before the first frame update
    void Start()
    {
        quitMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenLevel() // Start the game
    {
        SceneManager.LoadScene("Game");
        //SceneManager.LoadScene(1);
    }

    public void Close() // Quit Game if yes
    {
        Application.Quit();
        Debug.Log("Quitting Application");
    }
}
