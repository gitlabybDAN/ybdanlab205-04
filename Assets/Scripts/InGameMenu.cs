using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class InGameMenu : MonoBehaviour
{
    public GameObject inGamePanel;
    public GameObject pauseMenu;
    public GameObject optionMenu;
    public GameObject HUDPanel;
    public bool gamePaused;
    public PlayerMovement playerMov;
    public Toggle musicmute;
    public GameObject bgMusic;
    private AudioSource bgAudio;
    public TextMeshProUGUI percentage;
    public Image radialBar;
    public Image healthBar;
    public Image staminaBar;
    float point = 0;
    float speed = 10;
    // Start is called before the first frame update
    void Start()
    {
        gamePaused = false;
        inGamePanel.SetActive(false);
        pauseMenu.SetActive(true);
        optionMenu.SetActive(false);
        HUDPanel.SetActive(true);
        Time.timeScale = 1.0f;
        bgAudio = bgMusic.GetComponent<AudioSource>(); // reset everything if the game starts
    }

    // Update is called once per frame
    void Update()
    {
        if (musicmute.isOn) // mute toggle
        {
            bgAudio.mute = true;
        }
        else
        {
            bgAudio.mute = false;
        }

        if (Input.GetKeyDown(KeyCode.Escape)) // pause screen
        {
            gamePaused = true;
            inGamePanel.SetActive(true);
            HUDPanel.SetActive(false);
            Time.timeScale = 0.0f;
        }
        else
        {
            if (Input.GetKey(KeyCode.W) && point < 100)
            {
                point += Time.deltaTime * speed; // exp counts as much as player presses W
            }

            percentage.text = "EXP : " + Mathf.Round(point).ToString() + "%"; // exp numeral amount
            radialBar.fillAmount = point / 100.0f; // exp circle
            healthBar.fillAmount = playerMov.health / 5.0f; // health bar
            staminaBar.fillAmount = playerMov.stamina / 5.0f; // stamina bar
        }
    }

    public void ReturnMainMenu()
    {
        SceneManager.LoadScene("StartScreen"); // Return to StartScreen.
    }

    public void UnPause()
    {
        gamePaused = false;
        inGamePanel.SetActive(false);
        pauseMenu.SetActive(true);
        optionMenu.SetActive(false);
        HUDPanel.SetActive(true);
        Time.timeScale = 1.0f; // unpause let user back to the game with normal speed.
    }
}
